/**
 * Location - Section Rizor Hovertrax
 */
$('#menu-icon-open').click(function() {
    $('.dropdown-menu')
        .css({ 'display': 'block' })
        .animate({ 'width': '+=1197px' });

    setTimeout(function() {
        $('.dropdown-menu').animate({ 'height': '+=184px' });
    }, 500);

    $(this).css({ 'display': 'none' });
    $('#menu-icon-close').css({ 'display': 'block' });
});

$('#menu-icon-close').click(function() {
    $('.dropdown-menu').animate({ 'height': '-=184px' });

    setTimeout(function() {
        $('.dropdown-menu')
            .animate({ 'width': '-=1197px' })
            .css({ 'display': 'none' });
    }, 500);

    $(this).css({ 'display': 'none' });
    $('#menu-icon-open').css({ 'display': 'block' });
});

$('.dropdown-menu-navbar-link').click(function() {
    setTimeout(function() {
        $('.dropdown-menu').animate({ 'height': '-=184px' });

        setTimeout(function() {
            $('.dropdown-menu')
                .animate({ 'width': '-=1197px' })
                .css({ 'display': 'none' });
        }, 1500);
    });
});

$('.rizor-hovertrax-version').animate({
    'marginLeft': '+=183px',
    'opacity': '+=1'
}, 1500);

setTimeout(function() {
    if (window.matchMedia('(min-width: 481px)').matches) {
        $('.section-rizor-hovertrax-pointer-vertical img').animate({ 'height': '+=54px' });
        $('.section-rizor-hovertrax-pointer-horizontal img').animate({ 'width': '+=17px' });
    } else {
        $('.section-rizor-hovertrax-pointer-vertical img').animate({ 'height': '+=548px' });
        $('.section-rizor-hovertrax-pointer-horizontal img').animate({ 'width': '+=497px' });
    }

    setInterval(function() {
        $('.section-rizor-hovertrax-over-icon-play').animate({ 'opacity': '+=1' }, 1000);
        $('.section-rizor-hovertrax-over-icon-play').animate({ 'opacity': '-=0.8' }, 1000);
    });


    setTimeout(function() {
        if (window.matchMedia('(min-width: 481px)').matches) {
            $('.down-pointer img').animate({ 'height': '+=95px' });
        } else {
            $('.down-pointer img').animate({ 'height': '+=241px' });
        }

        setInterval(function() {
            $('.down-pointer img').animate({ 'opacity': '-=0.6' }, 800);
            $('.down-pointer img').animate({ 'opacity': '+=1' }, 800);
        });
    }, 900)
}, 1600);

/**
 * Location - Section Advantages
 */
setInterval(function() {
    $('.section-advantages-video-geroskuter div').animate({ 'opacity': '+=1' }, 1000);
    $('.section-advantages-video-geroskuter div').animate({ 'opacity': '-=0.4' }, 1000);
});

$('.section-advantages-design-color').animate({
    'marginTop': '-=300px',
    'opacity': '+=1'
}, 1500);

setTimeout(function() {
    if (window.matchMedia('(max-width: 480px)').matches) {
        $('.section-advantages-design-color-colorlist-pointer-horizontal img').animate({ 'width': '+=46px' }, 1000);
        $('.section-advantages-design-color-colorlist-pointer-vertical img').animate({ 'height': '+=356px' }, 1000);
    } else {
        $('.section-advantages-design-color-colorlist-pointer-horizontal img').animate({ 'width': '+=21px' }, 1000);
        $('.section-advantages-design-color-colorlist-pointer-vertical img').animate({ 'height': '+=213px' }, 1000);
    }
}, 1600);



/**
 * Location - Section Modes
 */
$('.section-modes-modeslist-paragraph').animate({
    'marginLeft': '+=225px',
    'opacity': '+=1'
}, 1500);

$('.section-modes-modeslist, p, .section-modes-mode-list-description-punktir-list img, .section-modes-modeslist-speed-mode-value, .section-modes-modeslist-speed-mode-description')
    .animate({ 'opacity': '+=1' }, 1500);

setTimeout(function() {
    $('.section-modes-modeslist-pointer-horizontal img').animate({ 'width': '+=134px' }, 1000);
    $('.section-modes-modeslist-pointer-vertical img').animate({ 'height': '+=256px' }, 1000);
}, 1600);

setInterval(function() {
    $('.section-modes-over-icon-play').animate({ 'opacity': '+=1' }, 1000);
    $('.section-modes-over-icon-play').animate({ 'opacity': '-=0.8' }, 1000);
});

/**
 * Location - Section For Whom
 */
if (window.matchMedia('(max-width: 480px)').matches) {
    $('.for-whom-info-list').animate({
        'marginTop': '-=350px',
        'opacity': '+=1'
    }, 1500);

    $('.section-for-whom-teenagers-img').animate({
        'marginTop': '-=300px'
    }, 1500);
} else {
    $('.for-whom-info-list').animate({
        'marginLeft': '+=275px',
        'opacity': '+=1'
    }, 1500);

    $('.section-for-whom-teenagers-img').animate({
        'marginTop': '-=121px'
    }, 1500);
}

setInterval(function() {
    $('.for-whom-info-list-btn-for-go-to-catalog').animate({ 'opacity': '-=0.8' }, 1000);
    $('.for-whom-info-list-btn-for-go-to-catalog').animate({ 'opacity': '+=1' }, 1000);
});