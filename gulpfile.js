let gulp         = require('gulp'),
    browserSync  = require('browser-sync'),
    autoprefixer = require('gulp-autoprefixer'),
    concat       = require('gulp-concat'),
    imagemin     = require('gulp-imagemin'),
    jsminify     = require('gulp-js-minify'),
    sass         = require('gulp-sass'),
    uglify       = require('gulp-uglify-es').default,
    watch        = require('gulp-watch');

gulp.task('browser-sync', function () {
    browserSync.init({
        server: {
            baseDir: "./"
        },
        notify: false
    });
});

gulp.task('sass', function() {
    return gulp.src('src/scss/main.scss')
        .pipe(sass().on('error', function (error) {
            console.log(error);
            alert(error);
        }))
        .pipe(gulp.dest('src/css'))
});

gulp.task('css', function() {
    return gulp.src('src/css/*.css')
        .pipe(concat('main.css'))
        .pipe(autoprefixer())
        .pipe(gulp.dest('build/css'))
});

gulp.task('js', function() {
    return gulp.src('src/js/*.js')
        .pipe(concat('main.js'))
        .pipe(uglify())
        .pipe(jsminify())
        .pipe(gulp.dest('build/js'))
});

gulp.task('jquery', function () {
    return gulp.src('node_modules/jquery/dist/jquery.js')
        .pipe(gulp.dest('build/js'))
});

gulp.task('imagemin', function() {
    return gulp.src('src/img/*.png')
        .pipe(imagemin())
        .pipe(gulp.dest('build/img'))
});

gulp.task('watch', function () {
    gulp.watch('src/scss/*.scss', gulp.parallel('sass'))
    gulp.watch('src/css/*.css', gulp.parallel('css'))
    gulp.watch('src/js/*.js', gulp.parallel('js'))
    gulp.watch('node_modules/jquery/dist/jquery.js', gulp.parallel('jquery'))
    gulp.watch('src/img/*.jpg', gulp.parallel('imagemin'))
});

exports.build = gulp.parallel('sass', 'css', 'js', 'jquery', 'imagemin');
exports.dev = gulp.parallel('browser-sync', 'watch');